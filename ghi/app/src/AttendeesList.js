import React from 'react';

function AttendeesList({ attendees }) {
  return (
    <table className="table table-striped mt-4">
      <thead>
        <tr>
          <th scope="col">ID</th>
          <th scope="col">Name</th>
          <th scope="col">Email</th>
        </tr>
      </thead>
      <tbody>
        {attendees.map((attendee) => (
          <tr key={attendee.id}>
            <td>{attendee.id}</td>
            <td>{attendee.name}</td>
            <td>{attendee.email}</td>
          </tr>
        ))}
      </tbody>
    </table>
  );
}

export default AttendeesList;
