import React from 'react';
import Nav from './Nav';
import AttendeesList from './AttendeesList';
// Import the ConferenceForm component
import ConferenceForm from './ConferenceForm';

function App(props) {
  if (props.attendees === undefined) {
    return null;
  }
  return (
    <>
      <Nav />
      <div className="container">
        {/* Commented out the LocationForm */}
        {/* <LocationForm /> */}
        {/* Render the ConferenceForm */}
        <ConferenceForm />
        <AttendeesList attendees={props.attendees} />
      </div>
    </>
  );
}

export default App;
