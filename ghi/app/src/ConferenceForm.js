import React, { useEffect, useState } from 'react';

function ConferenceForm(props) {
  const [conferenceData, setConferenceData] = useState({
    name: '',
    date: '',
    location: ''
  });

  const handleChange = (event) => {
    const { name, value } = event.target;
    setConferenceData({ ...conferenceData, [name]: value });
  };

  const handleSubmit = async (event) => {
    event.preventDefault();

    const conferenceUrl = 'http://localhost:8000/api/conferences/';
    const fetchConfig = {
      method: 'post',
      body: JSON.stringify(conferenceData),
      headers: {
        'Content-Type': 'application/json',
      },
    };

    const response = await fetch(conferenceUrl, fetchConfig);
    if (response.ok) {
      const newConference = await response.json();
      console.log(newConference);

      // Resetting the form values
      setConferenceData({
        name: '',
        date: '',
        location: ''
      });
    }
  };

  useEffect(() => {
    // Fetch any necessary data
  }, []);

  return (
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>Create a new conference</h1>
          <form onSubmit={handleSubmit} id="create-conference-form">
            <div className="form-floating mb-3">
              <input
                placeholder="Name"
                required
                type="text"
                id="name"
                className="form-control"
                name="name"
                value={conferenceData.name}
                onChange={handleChange}
              />
              <label htmlFor="name">Name</label>
            </div>
            <div className="form-floating mb-3">
              <input
                placeholder="Date"
                required
                type="date"
                id="date"
                className="form-control"
                name="date"
                value={conferenceData.date}
                onChange={handleChange}
              />
              <label htmlFor="date">Date</label>
            </div>
            <div className="form-floating mb-3">
              <input
                placeholder="Location"
                required
                type="text"
                id="location"
                className="form-control"
                name="location"
                value={conferenceData.location}
                onChange={handleChange}
              />
              <label htmlFor="location">Location</label>
            </div>
            <button className="btn btn-primary">Create</button>
          </form>
        </div>
      </div>
    </div>
  );
}

export default ConferenceForm;
